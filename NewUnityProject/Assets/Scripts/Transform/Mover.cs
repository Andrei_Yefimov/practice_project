﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    [SerializeField]
    Transform child;

    public float duration = 1f;
    public float rotateDuration = 1f;
    public float distance = 1f;
    public float yAngle = 45f;

    public void PrintStats()
    {
        Debug.Log("child.TransformDirection: " + child.TransformDirection(0, 0, 0));
        Debug.Log("child.TransformPoint: " + child.TransformPoint(0, 0, 0));
        Debug.Log("child.TransformVector: " + child.TransformVector(0, 0, 0));

        Debug.Log("child.InverseTransformDirection: " + child.InverseTransformDirection(0, 0, 0));
        Debug.Log("child.InverseTransformPoint: " + child.InverseTransformPoint(0, 0, 0));
        Debug.Log("child.InverseTransformVector: " + child.InverseTransformVector(0, 0, 0));

        Debug.Log("child.pos: " + child.position);
        Debug.Log("child.localPos: " + child.localPosition);

        Debug.Log("child.right: " + child.right);
        Debug.Log("child.forward: " + child.forward);
        Debug.Log("child.up: " + child.up);
    }

    public void MoveAlongX()
    {
        Debug.Log("child.right: "+child.right);
        StartCoroutine(Move(child.TransformDirection(distance, 0, 0), duration));
    }

    public void MoveAlongZ()
    {
        Debug.Log("child.right: " + child.forward);
        StartCoroutine(Move(child.TransformDirection(0, 0, distance), duration));
    }

    public void RotateAlongY()
    {
        Debug.Log("[RotateAlong Y]");
        StartCoroutine(Rotate(child.up, yAngle, rotateDuration));
    }
    public void RotateAlongInverseY()
    {
        Debug.Log("[RotateAlong -Y]");
        StartCoroutine(Rotate(child.up, -yAngle, rotateDuration));
    }

    IEnumerator Rotate(Vector3 axis, float angle, float duration)
    {
        Quaternion from = child.localRotation;
        Quaternion to = child.localRotation;
        to *= Quaternion.Euler(axis * angle);

        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            child.localRotation = Quaternion.Slerp(from, to, elapsed / duration);
            elapsed += Time.deltaTime;
            yield return null;
        }
        child.localRotation = to;
    }

    private void Update()
    {
        Debug.DrawRay(child.position, child.right * 5, Color.red);
        Debug.DrawRay(child.position, child.forward * 5, Color.blue);
        Debug.DrawRay(child.position, child.up * 5, Color.green);
    }

    IEnumerator Move(Vector3 axis, float duration = 1.0f)
    {
        Vector3 from = child.position;
        Vector3 to = child.position+axis;
        Debug.Log("from: " + from + " | to: " + to);

        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            child.position = Vector3.Lerp(from, to, elapsed / duration);
            elapsed += Time.deltaTime;
            yield return null;
        }
        child.position = to;
    }
}
