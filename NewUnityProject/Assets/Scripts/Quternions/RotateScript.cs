﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateScript : MonoBehaviour
{
    [SerializeField]
    float duration = 1.0f;
    [SerializeField]
    Vector3 idleState;
    [SerializeField]
    bool isIdleEmpty;
    [SerializeField]
    float xAngle, yAngle, zAngle;

    private void Start()
    {
        isIdleEmpty = false;
    }

    private void IdleCheck()
    {
        if (!isIdleEmpty)
        {
            idleState = transform.rotation.eulerAngles;
            isIdleEmpty = true;
        }
    }
    public void RotateAlongY()
    {
        Debug.Log("[RotateAlongY]");
        IdleCheck();
        StartCoroutine(Rotate(Vector3.up, yAngle, duration));
    }

    public void RotateAlongX()
    {
        Debug.Log("[RotateAlongX]");
        IdleCheck();
        StartCoroutine(Rotate(Vector3.right, xAngle, duration));
    }

    public void RotateAlongZ()
    {
        Debug.Log("[RotateAlongZ]");
        IdleCheck();
        StartCoroutine(Rotate(Vector3.forward, zAngle, duration));
    }

    IEnumerator Rotate(Vector3 axis, float angle, float duration = 1.0f)
    {
        Quaternion from = transform.rotation;
        Quaternion to = transform.rotation;
        to *= Quaternion.Euler(axis * angle);

        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            transform.rotation = Quaternion.Slerp(from, to, elapsed / duration);
            elapsed += Time.deltaTime;
            yield return null;
        }
        transform.rotation = to;
    }

    public void GetAngles()
    {
        if (isIdleEmpty)
        {
            float angleX;
            float angleY;
            float angleZ;

            angleX = Vector3.SignedAngle(idleState, transform.rotation.eulerAngles, Vector3.right);
            angleY = Vector3.SignedAngle(idleState, transform.rotation.eulerAngles, Vector3.up);
            angleZ = Vector3.SignedAngle(idleState, transform.rotation.eulerAngles, Vector3.forward);

            Debug.Log("Idle: " + idleState + " | Current: " + transform.rotation.eulerAngles + "\nX angle: " + angleX + "  | Y angle: " + angleY + " | Z angle: " + angleZ);
        }
        else Debug.Log("No rotation have been done.");
    }
}
