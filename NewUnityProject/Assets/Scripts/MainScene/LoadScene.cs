﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadScene : MonoBehaviour
{
    #region AssetBundles
    [SerializeField] Button switchScene;
    [SerializeField] Button playAudio;
    [SerializeField] Button pauseAudio;

    [SerializeField] AudioSource audioSource;
    [SerializeField] Image spriteRenderer;

    AssetBundle assetBundleScene;
    AssetBundle assetBundleAudio;
    public string sceneBundleUrl;
    public string audioBundleUrl;
    public string sceneAssetName = "content/scenes";
    public string audioAssetName = "content/ik-ey";
    public string sceneName = "Scene1";
    public Text infoText;



    private int version = 0;

    void Start()
    {
        switchScene.interactable = false;
        playAudio.interactable = false;
        pauseAudio.interactable = false;
        spriteRenderer.color = new Color(1, 1, 1, 0);
        sceneBundleUrl = Application.dataPath + "/AssetBundles/" + sceneAssetName;
        audioBundleUrl = Application.dataPath + "/AssetBundles/" + audioAssetName;

        //sceneBundleUrl = sceneAssetName;
        //sceneBundleUrl = "AssetBundles/"+sceneAssetName;
        //audioBundleUrl = audioAssetName;
    }

    public void OnClickLoadAudio()
    {
        StartCoroutine(DownloadAndCache());
    }

    IEnumerator DownloadAndCache()
    {
        string bundleUrl = "https://firebasestorage.googleapis.com/v0/b/newunityproject-6ff71.appspot.com/o/ik-ey?alt=media&token=85ac81e6-4dd2-474f-928a-330f8a1f6db6";
        UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(bundleUrl);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError) Debug.Log(www.error);
        else assetBundleAudio = DownloadHandlerAssetBundle.GetContent(www);

        Debug.Log("Bundle is downloaded.["+ (assetBundleAudio == null) + "]");
        string imageName = "ik-ey.jpeg";
        string audioName = "IK-EY x Theodor - Nasty.wav";

        var musicRequest = assetBundleAudio.LoadAssetAsync(audioName, typeof(AudioClip));
        yield return musicRequest;
        Debug.Log("Audio is downloaded.");

        var imageRequest = assetBundleAudio.LoadAssetAsync(imageName, typeof(Sprite));
        yield return imageRequest;
        Debug.Log("Image is downloaded.");

        audioSource.clip = musicRequest.asset as AudioClip;
        spriteRenderer.sprite = imageRequest.asset as Sprite;
        spriteRenderer.color = new Color(1, 1, 1, 1);
        if (spriteRenderer.sprite != null)
            spriteRenderer.preserveAspect = true;

        if (audioSource.clip != null)
        {
            playAudio.interactable = true;
            pauseAudio.interactable = true;
        }
    }

    IEnumerator DownloadAndCache__()
    {
        while (!Caching.ready)
            yield return null;
        assetBundleAudio = AssetBundle.LoadFromFile(audioBundleUrl);
        Debug.Log("Bundle is downloaded.");
        string imageName = "ik-ey.jpeg";
        string audioName = "IK-EY x Theodor - Nasty.wav";

        var musicRequest = assetBundleAudio.LoadAssetAsync(audioName, typeof(AudioClip));
        yield return musicRequest;
        Debug.Log("Audio is downloaded.");

        var imageRequest = assetBundleAudio.LoadAssetAsync(imageName, typeof(Sprite));
        yield return imageRequest;
        Debug.Log("Image is downloaded.");

        audioSource.clip = musicRequest.asset as AudioClip;
        spriteRenderer.sprite = imageRequest.asset as Sprite;
        spriteRenderer.color = new Color(1, 1, 1, 1);
        if (spriteRenderer.sprite != null)
            spriteRenderer.preserveAspect = true;

        if (audioSource.clip != null)
        {
            playAudio.interactable = true;
            pauseAudio.interactable = true;
        }
    }

    IEnumerator DownloadAndCache_()
    {
        while (!Caching.ready)
            yield return null;
        var www = WWW.LoadFromCacheOrDownload(audioBundleUrl, version);
        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log(www.error);
            yield break;
        }
        Debug.Log("Bundle is downloaded. [" + (www.assetBundle == null) + "]");
        assetBundleAudio = www.assetBundle;
        string imageName = "ik-ey.jpeg";
        string audioName = "IK-EYxTheodor-Nasty.wav";

        var musicRequest = assetBundleAudio.LoadAssetAsync(audioName, typeof(AudioClip));
        yield return musicRequest;
        Debug.Log("Audio is downloaded.");

        var imageRequest = assetBundleAudio.LoadAssetAsync(imageName, typeof(Sprite));
        yield return imageRequest;
        Debug.Log("Audio is downloaded.");

        audioSource.clip = musicRequest.asset as AudioClip;
        spriteRenderer.sprite = imageRequest.asset as Sprite;
    }

    public void PlayAudio()
    {
        audioSource.Play();
    }

    public void PauseAudio()
    {
        audioSource.Pause();
    }

    public void LoadAssetBundle()
    {
        assetBundleScene = AssetBundle.LoadFromFile(sceneBundleUrl);

        infoText.text = assetBundleScene != null
            ? "Asset is loaded"
            : "Something went wrong";

        if(assetBundleScene != null)
            switchScene.interactable = true;

        print(sceneBundleUrl);

    }

    public void SwitchScene()
    {
        audioSource.Stop();
        string[] scenePaths = assetBundleScene.GetAllScenePaths();
        string sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePaths[0]);
        SceneManager.LoadScene(sceneName);
    }
    #endregion

    #region PlayerPrefs
    [SerializeField]
    private string playerPrefsKey = "Input";
    [SerializeField]
    private InputField inputField;

    private void OnStart()
    {
        if (PlayerPrefs.HasKey(playerPrefsKey))
            infoText.text = "[old: ]" + PlayerPrefs.GetString(playerPrefsKey);
        else infoText.text = "Welcome!";
    }

    public void SaveData(string input)
    {
        if (input.Length < 3)
        {
            infoText.text = "Input is got\n{" + inputField.text + "}";
        }
        else infoText.text = "Too few letters...";
    }

    public void ReadData()
    {
        if (PlayerPrefs.HasKey(playerPrefsKey))
        {
            string s = PlayerPrefs.GetString(playerPrefsKey);
            if (s == null || s.Length == 0)
                infoText.text = "Key is empty";
            else
                infoText.text = "Key: " + s;
        } else
        {
            infoText.text = "Key is empty(or deleted)";
        }

    }

    public void DeleteKey()
    {
        PlayerPrefs.DeleteKey(playerPrefsKey);
        infoText.text = "Key is deleted.";
    }

    public void SaveInputData()
    {
        PlayerPrefs.SetString(playerPrefsKey, inputField.text);
        infoText.text = "Saving...";
    }
    #endregion
}
