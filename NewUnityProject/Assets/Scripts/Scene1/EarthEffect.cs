﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EarthEffect : MonoBehaviour
{
    [SerializeField]
    Text effectTimer;
    [SerializeField]
    private float timer = 1f;
    [SerializeField]
    ParticleSystem earthEffect;

    void Start()
    {
        earthEffect.gameObject.SetActive(true);
        earthEffect.Stop();
        StartCoroutine(InitEffectWithDelay());
    }

    void Update()
    {
        //print((int)(1f / Time.unscaledDeltaTime));
    }

    IEnumerator InitEffectWithDelay()
    {
        float _timer = timer;
        while(_timer > 0)
        {
            effectTimer.text = "Something will happen in\n" + _timer;
            _timer -= Time.deltaTime;
            _timer = (float)Math.Round(_timer, 3);
            yield return null;
        }

        effectTimer.enabled = false;
        earthEffect.Play();
        yield return new WaitForSeconds(1f);
        earthEffect.Stop();
        effectTimer.text = "Ta-daaaam";
        Debug.Log("Earth effect is spawned.");
    }

    
}
