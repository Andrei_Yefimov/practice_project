﻿using UnityEditor;

public class PackToAssetBundle
{
    [MenuItem("Assets/Build AssetBundle *")]
    static void BuildBundles()
    {
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles", BuildAssetBundleOptions.None, BuildTarget.iOS);
    }
}
